# Copyright © 2021 kitzman <kitzman @ disroot . org>
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{4..9} )
inherit distutils-r1

DESCRIPTION="Sophisticated chroot/build/flash tool to develop and install postmarketOS."
HOMEPAGE="https://gitlab.com/postmarketOS/pmbootstrap"
SRC_URI="https://gitlab.com/postmarketOS/pmbootstrap/-/archive/${PV}/${PN}-${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
REQUIRED_USE=${PYTHON_REQUIRED_USE}

DEPEND="dev-libs/openssl:0="
RDEPEND="${DEPEND}"
