# Copyright © 2021 kitzman <kitzman @ disroot . org>
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="The BitTorrent Protocol Daemon"
HOMEPAGE="github.com/btpd/btpd/wiki"
EGIT_REPO_URI="https://github.com/${PN}/${PN}"

LICENSE="BSD-4"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="dev-libs/openssl:0="
RDEPEND="${DEPEND}"

src_configure() {
	econf
}

src_compile() {
	emake
}

src_install() {
	default
}
