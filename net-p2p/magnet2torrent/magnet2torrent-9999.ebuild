# Copyright © 2021 kitzman <kitzman @ disroot . org>
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{4..9} )
inherit python-single-r1

DESCRIPTION="A command line tool that converts magnet links in to .torrent files."
HOMEPAGE="https://github.com/danfolkes/Magnet2Torrent"
EGIT_REPO_URI="https://github.com/danfolkes/Magnet2Torrent"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
REQUIRED_USE=${PYTHON_REQUIRED_USE}

DEPEND="dev-libs/openssl:0="
RDEPEND="${DEPEND}"

src_compile() {
	default
}

src_install() {
	default
}
