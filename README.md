# Synopsis

This is Kitzman's Gentoo Overlay. Doesn't contain much packages, but it's something. :)

# Package list

Packages present in this overlay are:

  - btpd
  - radare2
  - magnet2torrent
  - procs
  - elinks (with spidermonkey, gemini)
  - pmbootstrap (for postmarketOS)
  - msm8960 kernel headers (tuned for use with crossdev)
  - miraclecast - no systemd version (not really working, eh)
  - toolchains for armv6 (newlib), armv7l (musl), mips (musl), aarch64 (musl), x86_64 (musl)
