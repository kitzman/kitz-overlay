# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit flag-o-matic toolchain-funcs

DESCRIPTION="Multipurpose relay (SOcket CAT)"
HOMEPAGE="https://git.9front.org/plan9front/purgatorio/"
SRC_URI="https://git.9front.org/git/plan9front/purgatorio/HEAD/snap.tar.gz"
S="${WORKDIR}/purgatorio"

PATCHES=(
	"${FILESDIR}"/0000-fix-multiple-definition-of.patch
	"${FILESDIR}"/0001-fix-no-gui.patch
)

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 arm arm64 x86 ~amd64-linux ~x86-linux"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

PURGATORIO="$S"

OBJTYPE=""

pkg_setup() {
	elog "You can find the purgatorio installation in /opt/purgatorio"
	elog "Ideally this would be owned by purgatorio:purgatorio"
}

src_prepare() {
	case $ARCH in
		amd64)
			OBJTYPE=386
			;;
		x86)
			OBJTYPE=386
			;;
		arm)
			OBJTYPE=arm
			;;
		arm64)
			OBJTYPE=arm
			;;
		*)
			elog "unknown object type"
			exit 1
			;;
	esac

	default
}

src_configure() {
	echo "ROOT=$PURGATORIO" >$S/mkconfig
	echo 'TKSTYLE=std' >>$S/mkconfig
	echo 'CONF=emu-g' >>$S/mkconfig
	echo 'SYSHOST=Linux' >>$S/mkconfig
	echo 'SYSTARG=Linux' >>$S/mkconfig

	echo "OBJTYPE=$OBJTYPE" >>$S/mkconfig

	echo 'OBJDIR=$SYSTARG/$OBJTYPE' >>$S/mkconfig

	echo '<$ROOT/mkfiles/mkhost-$SYSHOST' >>$S/mkconfig
	echo '<$ROOT/mkfiles/mkfile-$SYSTARG-$OBJTYPE' >>$S/mkconfig
}

src_compile() {
	PATH=$S/Linux/$OBJTYPE/bin:$PATH

	cd $S

	./makemk.sh

	mk mkdirs
	mk install
}

src_install() {
	mkdir -p "$D"/opt
	cp -r "$S" "$D"/opt
}
